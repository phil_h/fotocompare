#!/usr/bin/env python3
# -*- coding: utf-8 -*-

from PIL import Image
import time
import os, subprocess, os.path, sys
from glob import glob
from random import randint
from elo import rate_1vs1
def compare(aPath,bPath):
        
        image1 = Image.open(aPath)
        image2 = Image.open(bPath)
        (width1, height1) = image1.size
        (width2, height2) = image2.size

        result_width = width1 + width2 + 50
        result_height = max(height1, height2)

        result = Image.new('RGB', (result_width, result_height))
        result.paste(im=image1, box=(0, 0))
        result.paste(im=image2, box=(width1+50, 0))
        
        result.show()
        time.sleep(3)
        #os.system("""osascript -e 'tell app "Terminal" to activate'""")
        answer = subprocess.check_output(['osascript', '-e', \
               r'''display dialog "Enter your choice here..." buttons {"Left", "Right","im done for now"}'''])
        aw = answer[16:-1]
        os.system("""osascript -e 'tell app "Preview" to close front window'""")
        if 'Right' in str(aw):
            win = 2
        elif 'Left' in str(aw):
            win = 1
        elif 'done' in str(aw):
            sys.exit()
        print(win)
        osCommand(aPath,bPath,win)
        
            
pass

def random(b):
    anum = randint(0,b)
    bnum = randint(0,b)
    if (anum!=bnum):
        return anum,bnum
    else:
        random(b)
pass

    
def checkRand():
    files_list = glob(os.path.join('/Users/phil/ownCloud/Photos/Abiball-CPG/', '*.jpg'))
    if len(files_list) < 2:
        files_list = glob(os.path.join('/Users/phil/Desktop/Bewertet/', '*.jpg'))
    print("[DEBUG]  len(files_list) = " + str(len(files_list)))
    a,b = random(len(files_list)-1)
    files = files_list
    bnum = int(b)
    aPath = files[a]
    bPath = files[b]
    print ("öffne " + aPath)
    print ("öffne " + bPath)

    if (os.path.isfile(aPath) == True and os.path.isfile(bPath) == True ):
       compare(aPath,bPath)
       
 
    
pass
def rate(aPath):
    try:
        astr = aPath.split("t/")
        aa= astr[1].split("_")
        return float(aa[0])
    except:
        return 1000
pass

def getNumber(aPath):
    astr = aPath.split("t/")
    aa= astr[1].split("_")
    a = aa[1].replace(".jpg","") 
    return int(a)

def osCommand(aPath,bPath,win):
    try:
        anzahl = len([name for name in os.listdir('/Users/phil/Desktop/Bewertet/')])
    except FileNotFoundError:
        os.system("mkdir ~/Desktop/Bewertet")
        anzahl = len([name for name in os.listdir('/Users/phil/Desktop/Bewertet/')])
    print("[DEBUG]  " + str(anzahl))
    A,B = 0,0
    if(win==1):
        A,B = rate_1vs1(rate(aPath), rate(bPath))
    else:
        B,A = rate_1vs1(rate(bPath), rate(aPath))
    aCommand = "mv " + aPath + " ~/Desktop/Bewertet/" + str(A) + "_" + str(getNumber(aPath)) + ".jpg"  
    os.system(aCommand)
    aCommand = "mv " + bPath + " ~/Desktop/Bewertet/" + str(B) + "_" + str(getNumber(bPath)) + ".jpg"  
    os.system(aCommand)
    

while(True):
    checkRand()